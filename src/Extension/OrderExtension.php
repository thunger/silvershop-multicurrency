<?php

namespace Thunger\SilverShopMultiCurrency\Extension;

use SilverShop\Extension\ShopConfigExtension;
use SilverShop\Page\AccountPage;
use SilverStripe\Control\Controller;
use SilverStripe\Core\Config\Config;
use SilverStripe\Dev\TaskRunner;
use SilverStripe\Forms\CompositeField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Omnipay\GatewayInfo;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\FieldType\DBMoney;
use SilverStripe\Security\Security;
use Thunger\SilverShopMultiCurrency\Service\CurrencyConverter;

class OrderExtension extends DataExtension {

	private static $db = [
		'StoredCurrency' => 'Varchar(3)', // cant be "Currency" because of core method
	];

	private static $summary_fields = [
		'StoredCurrency' => 'Currency',
	];

	public function onBeforeWrite() {
		parent::onBeforeWrite();
		$currency = $this->owner->getField('StoredCurrency');
		if(!$currency || $this->owner->IsCart()) {
			$this->owner->setField(
				'StoredCurrency',
				ShopConfigExtension::get_site_currency()
			);
		}
	}

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldToTab(
			'Root.Main',
			CompositeField::create(
				LiteralField::create('CurrencyInfo', $this->getStoredCurrency())
			)->setTitle('Currency'),
			'Customer'
		);
	}

	public function getStoredCurrency() {
		$currency = $this->owner->getField('StoredCurrency');
		if (!$currency) {
			$currency = ShopConfigExtension::get_site_currency();
		}
		return $currency;
	}

	public function TotalOutstandingMoney() {
		return DBMoney::create()
			->setCurrency($this->owner->getStoredCurrency())
			->setAmount($this->owner->TotalOutstanding());
	}

	public function SubTotalMoney() {
		return DBMoney::create()
			->setCurrency($this->owner->getStoredCurrency())
			->setAmount($this->owner->SubTotal());
	}

	public function TotalMoney() {
		return DBMoney::create()
			->setCurrency($this->owner->getStoredCurrency())
			->setAmount($this->owner->Total());
	}

	public function ShippingMoney() {
		return DBMoney::create()
			->setCurrency($this->owner->getStoredCurrency())
			->setAmount($this->owner->ShippingTotal);
	}

	public function TotalPaidMoney() {
		return DBMoney::create()
			->setCurrency($this->owner->getStoredCurrency())
			->setAmount($this->owner->TotalPaid());
	}

	public function checkCurrency() {
		$siteCurrency = ShopConfigExtension::get_site_currency();
		if ($this->owner->StoredCurrency != $siteCurrency) {
			$origCurrency = $this->owner->StoredCurrency;
			$this->owner->setField(
				'StoredCurrency',
				$siteCurrency
			);
			$this->owner->write();
			foreach ($this->owner->Items() as $Item) {
				$Item->setField(
					'UnitPrice',
					$Item->Product()->sellingPrice()
				);
				$Item->write();
			}
			foreach ($this->owner->Modifiers() as $Modifier) {
				$Modifier->setField(
					'Amount',
					CurrencyConverter::convert($Modifier->Amount, $origCurrency, $siteCurrency)
				);
				$Modifier->write();
			}
			$this->owner->calculate();
			$this->owner->write();
		}
	}

	public function beforeAdd($buyable, $quantity, $filter) {
		$this->owner->checkCurrency();
	}

	public function beforeSetQuantity($buyable, $quantity, $filter) {
		$this->owner->checkCurrency();
	}

	public function beforeRemove($buyable, $quantity, $filter) {
		$this->owner->checkCurrency();
	}

}