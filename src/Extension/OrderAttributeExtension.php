<?php

namespace Thunger\SilverShopMultiCurrency\Extension;

use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\FieldType\DBMoney;

class OrderAttributeExtension extends DataExtension {

	public function TableValueMoney() {
		return DBMoney::create()
			->setCurrency($this->owner->Order()->getStoredCurrency())
			->setAmount($this->owner->TableValue());
	}

	public function UnitPriceMoney() {
		return DBMoney::create()
			->setCurrency($this->owner->Order()->getStoredCurrency())
			->setAmount($this->owner->UnitPrice());
	}

	public function TotalMoney() {
		return DBMoney::create()
			->setCurrency($this->owner->Order()->getStoredCurrency())
			->setAmount($this->owner->Total());
	}

}