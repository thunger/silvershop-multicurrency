<?php

namespace Thunger\SilverShopMultiCurrency\Extension;

use SilverShop\Extension\ShopConfigExtension;
use SilverStripe\Core\Config\Config;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\Forms\GridField\GridFieldDetailForm;
use SilverStripe\Forms\GridField\GridFieldToolbarHeader;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\DataObject;
use Symbiote\GridFieldExtensions\GridFieldEditableColumns;
use Thunger\SilverShopMultiCurrency\Model\Price;

class PricingExtension extends DataExtension {

	public function updateSummaryFields(&$fields) {
		parent::updateSummaryFields($fields);
		if (isset($fields['Price'])) {
			unset($fields['Price']);
		}
	}

	public static function get_gridfield_config() {
		$config = GridFieldConfig::create();
		$config->addComponent($columns = new GridFieldEditableColumns());
		$config->addComponent(new GridFieldDetailForm());
		$config->addComponent(new GridFieldToolbarHeader());
		$columns->setDisplayFields(
			[
				'MoneyCurrency' => array(
					'field' => ReadonlyField::class
				),
				'MoneyAmount' => function ($record, $column, $grid) {
					return new TextField($column);
				}
			]
		);
		return $config;
	}

	public function updateCMSFields(FieldList $fields) {
		$fields->removeByName('BasePrice');
		$fields->removeByName('Price');
	}

	public function onAfterWrite() {
		// make sure we have all prices in all currencies
		// create if needed
		foreach (Price::get_currencies() as $Currency) {
			foreach ($this->owner->priceRelationNames() as $priceRelationName => $priceRelationField) {
				$this->owner->PriceObjectForCurrency($Currency, $priceRelationName);
			}
		}
	}

	public function priceRelationNames($search = null) {
		$relationNames = [];
		foreach ($this->owner->hasMany(false) as $key => $value) {
			if (strpos($value, Price::class) === 0) {
				$relationNames[$key] = str_replace(Price::class.'.', '', $value);
			}
		}
		if ($search) {
			if (isset($relationNames[$search])) {
				return $relationNames[$search];
			}
		} else {
			return $relationNames;
		}
	}

	public function PriceObjectForCurrency($Currency = null, $RelationName = 'Prices') {
		if (is_null($Currency)) {
			$Currency = Config::inst()->get(ShopConfigExtension::class, 'base_currency');
		}
		if ($rel = $this->priceRelationNames($RelationName)) {
			$PriceObject = DataObject::get_one(
				Price::class,
				[
					$rel.'ID' => $this->owner->ID,
					'MoneyCurrency' => $Currency
				]
			);
		} else {
			$PriceObject = $this->owner->$RelationName()->filter(
				[
					'MoneyCurrency' => $Currency
				]
			)->first();
		}
		if (!$PriceObject) {
			$PriceObject = Price::create();
			$PriceObject->MoneyAmount = 0;
			$PriceObject->MoneyCurrency = $Currency;
			$PriceObject->write();
			$this->owner->$RelationName()->add($PriceObject);
		}
		return $PriceObject;
	}

	public function PriceForCurrency($Currency = null, $RelationName = 'Prices') {
		return $this->owner->PriceObjectForCurrency($Currency, $RelationName)->obj('Money');
	}

	public function PriceAmountForCurrency($Currency = null, $RelationName = 'Prices') {
		return $this->owner->PriceObjectForCurrency($Currency, $RelationName)->MoneyAmount;
	}

	public function setPriceForCurrency($Price, $Currency, $RelationName = 'Prices') {
		$PriceObject = $this->owner->PriceObjectForCurrency($Currency, $RelationName);
		$PriceObject->MoneyAmount = $Price;
		$PriceObject->write();
	}

	public function ListPrices($RelationName = 'Prices') {
		$return = [];
		foreach ($this->owner->$RelationName() as $Price) {
			$return[] = $Price->obj('Money')->Nice();
		}
		return implode('/', $return);
	}

}
