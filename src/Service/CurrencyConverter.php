<?php

namespace Thunger\SilverShopMultiCurrency\Service;


use Psr\SimpleCache\CacheInterface;
use SilverStripe\Core\Injector\Injector;

class CurrencyConverter {
	private static function getTable() {
		$cache = Injector::inst()->get(CacheInterface::class.'.currencyConversionCache');
		$cacheKey = 'conversionRates';
		if (!$cache->has($cacheKey)) {
			$cache->set($cacheKey, self::retrieveRates(), 3600 * 24);
		}
		return $cache->get($cacheKey);
	}

	private static function retrieveRates() {
		$rates = [];
		try {
			$XML = simplexml_load_file('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml');
			foreach ($XML->Cube->Cube->Cube as $rate) {
				$rates[$rate['currency']->__toString()] = (float)$rate['rate']->__toString();
			}
		} catch (\Exception $e) {

		}
		return $rates;
	}

	// converts from $from to EUR and then to $to :-)
	public static function convert($amount, $from, $to) {
		$rates = self::getTable();
		if ($from != 'EUR' && isset($rates[$from])) {
			$amount = $amount / $rates[$from];
		}
		if ($to != 'EUR' && isset($rates[$to])) {
			$amount = $amount * $rates[$to];
		}
		return $amount;
	}
}