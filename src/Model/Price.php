<?php

namespace Thunger\SilverShopMultiCurrency\Model;

use SilverShop\Extension\ShopConfigExtension;
use SilverStripe\Core\Config\Config;
use SilverStripe\ORM\DataObject;

class Price extends DataObject {

	private static $db = [
		'Money' => 'Money'
	];

	private static $indexes = [
		'MoneyAmount' => true,
		'MoneyCurrency' => true
	];

	private static $table_name = 'SilverShop_Price';

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->dataFieldByName('Money')->setAllowedCurrencies(Price::get_currencies());
		return $fields;
	}

	public static function get_currencies() {
		$Currencies = [];
		$LocaleConfigs = Config::inst()->get('LocaleConfig');
		foreach ($LocaleConfigs as $LocaleConfig) {
			if (
				isset($LocaleConfig[ShopConfigExtension::class])
				&& isset($LocaleConfig[ShopConfigExtension::class]['base_currency'])
			) {
				$Currencies[] = $LocaleConfig[ShopConfigExtension::class]['base_currency'];
			}
		}
		return array_combine($Currencies, $Currencies);
	}

	public function canEdit($member = null) {
		foreach ($this->config()->get('has_one') as $hasOne => $className) {
			if ($this->$hasOne()->exists()) {
				return $this->$hasOne()->canEdit($member);
			}
		}
	}
}